package ru.vpavlova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.vpavlova.tm.api.repository.IRepository;
import ru.vpavlova.tm.dto.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IRepository<Task> {

    @Modifying
    @Query("DELETE FROM Task e")
    void clear();

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    @NotNull
    List<Task> findAllByUserId(@Nullable String userId);

    @NotNull
    Optional<Task> findOneByIdAndUserId(
            @Nullable String userId, @NotNull String id
    );

    @NotNull
    Optional<Task> findOneByUserIdAndName(
            @Nullable String userId, @NotNull String name
    );

    void removeByUserId(@NotNull String userId);

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :taskId")
    void bindTaskByProjectId(
            @Param("userId") @NotNull String userId,
            @Param("projectId") @NotNull String projectId,
            @Param("taskId") @NotNull String taskId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskFromProjectId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeOneByIdAndUserId(@Nullable String userId, @NotNull String id);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.userId = :userId and e.name = :name")
    void removeOneByName(
            @Param("userId") @NotNull String userId,
            @Param("name") @NotNull String name
    );

}
