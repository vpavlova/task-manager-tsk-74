package ru.vpavlova.tm.exception;

public class ProcessException extends AbstractException {

    public ProcessException() {
        super("Process error! Try again.");
    }

}
