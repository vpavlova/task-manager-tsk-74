package ru.vpavlova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.vpavlova.tm.exception.ProcessException;
import ru.vpavlova.tm.model.CustomUser;

import java.util.Optional;


public class UserUtil {

    public static String getUserId() {
        @NotNull final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        @Nullable Object principal = auth.getPrincipal();
        Optional.ofNullable(principal).orElseThrow(ProcessException::new);
        if (!(principal instanceof CustomUser)) {
            throw new ProcessException();
        }
        @NotNull
        CustomUser customerUser = (CustomUser) principal;
        return customerUser.getUserId();
    }

}