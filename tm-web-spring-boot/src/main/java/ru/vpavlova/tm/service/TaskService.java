package ru.vpavlova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.service.ITaskService;
import ru.vpavlova.tm.exception.EmptyIdException;
import ru.vpavlova.tm.model.Task;
import ru.vpavlova.tm.repository.ITaskRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    public void addAll(final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            save(item);
        }
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<Task> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (Task item : collection) {
            item.setUserId(userId);
            save(item);
        }
    }

    @Override
    public Task save(final Task entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task save(final String userId, @Nullable final Task entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @Nullable final Task entityResult = save(entity);
        return entityResult;
    }

    @Override
    public void create(final String userId) {
        save(userId, new Task("task", "new"));
    }

    @Override
    public void create() {
        repository.save(new Task("task", "new"));
    }

    @Override
    @Transactional
    public Task create(@Nullable Task task) {
        return repository.save(task);
    }

    @Override
    public Task findById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public Task findByIdAndUserId(@Nullable String id, @Nullable String userId) {
        return repository.findByIdAndUserId(id, userId);
    }

    @Override
    public Collection<Task> findAllByCurrentUserId(@Nullable String userId) {
        return repository.findAllByCurrentUserId(userId);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public void clearAllByCurrentUserId(@Nullable String userId) {
        repository.removeAllByCurrentUserId(userId);
    }

    @Override
    public void removeById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteByUserIdAndId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void remove(final Task entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

    @Override
    public void removeByIdAndUserId(@Nullable String id, @Nullable String userId) {
        repository.removeByIdAndUserId(id, userId);
    }

}