package ru.vpavlova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.vpavlova.tm.model.Result;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface IAuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    Result login(
            @WebParam(name = "username") @RequestParam("username") @NotNull final String username,
            @WebParam(name = "password") @RequestParam("password") @NotNull final String password
    );

    @WebMethod
    @GetMapping("/logout")
    Result logout();

    @WebMethod
    @GetMapping("/profile")
    Result profile();

}
