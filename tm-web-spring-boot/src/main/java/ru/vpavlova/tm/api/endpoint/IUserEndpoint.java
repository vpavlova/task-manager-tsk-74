package ru.vpavlova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.vpavlova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/users")
public interface IUserEndpoint {

    @WebMethod
    @PostMapping("/create")
    void create(@PathVariable("login") @NotNull String login, @PathVariable("password") @NotNull String password);

    @WebMethod
    @PostMapping("/deleteByLogin/{login}")
    void deleteByLogin(@PathVariable("login") @NotNull String login);

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") @NotNull String id);

    @WebMethod
    @GetMapping("/findById/{id}")
    User find(@PathVariable("id") @NotNull String id);

    @WebMethod
    @GetMapping("/findAll")
    Collection<User> findAll();

    @WebMethod
    @PostMapping("/save")
    void save(@RequestBody @NotNull User user);

}

