package ru.vpavlova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vpavlova.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(@NotNull String userId);

    void addAll(Collection<Project> collection);

    void addAll(String userId, @Nullable Collection<Project> collection);

    Project save(Project entity);

    Project save(String userId, @Nullable Project entity);

    void create();

    void create(String userId);

    Project create(@Nullable Project project);

    Project findById(String id);

    Project findById(@NotNull String userId, @Nullable String id);

    Project findByIdAndUserId(@Nullable String id, @Nullable String userId);

    Collection<Project> findAllByCurrentUserId(@Nullable String userId);

    void clear();

    void clear(@NotNull String userId);

    void clearAllByCurrentUserId(@Nullable String userId);

    void removeById(String id);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(Project entity);

    void removeByIdAndUserId(@Nullable String id, @Nullable String userId);

}