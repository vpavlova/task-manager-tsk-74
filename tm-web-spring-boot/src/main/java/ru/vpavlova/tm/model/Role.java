package ru.vpavlova.tm.model;

import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.vpavlova.tm.enumerated.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Setter
@Getter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_role")
@XmlAccessorType(XmlAccessType.FIELD)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Role extends AbstractEntity {

    @NotNull
    @Column(name = "roletype")
    @Enumerated(EnumType.STRING)
    private RoleType role = RoleType.USER;

    @ManyToOne
    private User user;

    @Override
    public String toString() {
        return role.toString();
    }

}
