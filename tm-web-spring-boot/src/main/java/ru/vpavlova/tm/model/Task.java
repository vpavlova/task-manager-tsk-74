package ru.vpavlova.tm.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class Task extends AbstractBusinessEntity {

    private String projectId;

    public Task(final String name) {
        this.name = name;
    }

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}
