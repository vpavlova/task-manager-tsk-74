package ru.vpavlova.tm.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractBusinessEntity {

    public Project(final String name) {
        this.name = name;
    }

    public Project(String name,final String description) {
        this.name = name;
        this.description = description;
    }

}
