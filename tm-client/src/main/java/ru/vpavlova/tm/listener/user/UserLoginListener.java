package ru.vpavlova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractUserListener;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.UserNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

@Component
public class UserLoginListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Login to program.";
    }

    @Override
    @EventListener(condition = "@userLoginListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        Session session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new UserNotFoundException();
        sessionService.setSession(session);
    }

}
