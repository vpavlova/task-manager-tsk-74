package ru.vpavlova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class SiteConst {

    @NotNull
    public static final String site = "http://localhost:8080";

    @NotNull
    public static final String projects_address = "/api/projects";

    @NotNull
    public static final String tasks_address = "/api/tasks";

}
